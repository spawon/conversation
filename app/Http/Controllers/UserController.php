<?php

namespace App\Http\Controllers;

use App\Conversation;
//use App\Events\NewMessage;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function get()
    {
        $users = User::where('id', '!=', Auth::user()->id)->get();

        return response()->json($users);
    }

    public function getMessage($id)
    {
        $messages = Conversation::where('from', $id)->orWhere('to', $id)->get();

        return response()->json($messages);
    }

    public function send(Request $request)
    {
        $message = Conversation::create([
            'from' => Auth::user()->id,
            'to' => $request->user_id,
            'message' => $request->message
        ]);

//        broadcast(new NewMessage($message));

        return response()->json($message);
    }
}
