<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConversationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        for ($i = 0; $i < 100; $i++) {

            DB::table('conversations')->insert([
                'from' => rand(1, 8),
                'to' => rand(9, 15),
                'message' => Str::random(100)
            ]);
        }
    }
}
