<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Spawon',
            'email' => 'spawon@nxt.ru',
            'profile_image' => 'https://avatariki.ru/images/trueimg/pictures/62/1C93194C05BE-62.gif',
            'password' => bcrypt('12345678'),
        ]);

        for ($i = 0; $i < 14; $i++)
            DB::table('users')->insert([
                'name' => Str::random(10),
                'email' => Str::random(10) . '@gmail.com',
                'profile_image' => 'https://avatariki.ru/images/trueimg/pictures/62/1C93194C05BE-62.gif',
                'password' => bcrypt('12345678'),
            ]);
    }
}
