@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col col-sm-12">
                <div class="card">
                    <div class="card-header">Conversation app</div>
                    <div class="card-body" id="app">
                        <app :user="{{Auth::user()}}"></app>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
